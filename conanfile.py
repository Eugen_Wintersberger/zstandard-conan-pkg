from conans import ConanFile, CMake, tools
import os


class ZstandardConan(ConanFile):
    name = "zstandard"
    version = "1.2.0"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = "*.tar.gz"

    def source(self):
        archive_file = "zstd-1.2.0.tar.gz"
        
        tools.unzip(archive_file)

    def configure(self):
        pass

    def build(self):
        if self.settings.compiler == "Visual Studio":
            sln_file = os.path.join("zstd-1.2.0","build","VS2010","zstd.sln")
            build_command = tools.build_sln_command(self.settings,sln_file,targets=["libzstd-dll"])
            command = "%s && %s" %(tools.vcvars_command(self.settings),build_command)
            self.run(command)

    def package(self):
        self.copy("*.h", dst="include", src=os.path.join("zstd-1.2.0","lib"))
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["zstd"]
